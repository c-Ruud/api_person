package com.example.api_person;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class WebClientHandler {
    private static Mono<String> token;
    WebClient.Builder webClient;

    public WebClientHandler(WebClient.Builder webClient) {
        this.webClient = webClient;
    }

    public static Mono<KeyCloakToken> acquire(String keyCloakBaseUrl, String realm, String clientId, String username, String password) {
        WebClient webClient = WebClient.builder()
                .baseUrl(keyCloakBaseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();
        Mono<KeyCloakToken> keyCloakToken = webClient.post()
                .uri("auth/realms/" + realm + "/protocol/openid-connect/token")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(BodyInserters.fromFormData("grant_type", "password")
                        .with("client_id", clientId)
                        .with("username", username)
                        .with("password", password)
                        .with("access_token", ""))
                .retrieve()
                .bodyToFlux(KeyCloakToken.class)
                .onErrorMap(e -> new Exception("Failed to aquire token", e))
                .last();
        token = keyCloakToken.map(KeyCloakToken::getAccessToken);
        return keyCloakToken;
    }

    public Group createGroup(String name) {
        return webClient.build()
                .post()
                .uri("http://localhost:8084/groups/create?name=" + name)
                .header("keycloak", "Bearer " + token)
                .retrieve()
                .bodyToMono(Group.class)
                .block();
    }

    public List<Group> getGroups() {
        return webClient.build()
                .get()
                .uri("http://localhost:8084/groups/all")
                .header("keycloak", "Bearer " + token)
                .retrieve()
                .bodyToFlux(Group.class).collectList()
                .block();
    }

    public Group getGroupById(String id) {
        return webClient.build()
                .get()
                .uri("http://localhost:8084/groups/get/" + id)
                .header("keycloak", "Bearer " + token)
                .retrieve()
                .bodyToMono(Group.class)
                .block();
    }

    public Group updateGroup(String id, String newName) {
        return webClient.build()
                .put()
                .uri("http://localhost:8084/groups/update/" + id + "?newName=" + newName)
                .header("keycloak", "Bearer " + token)
                .retrieve()
                .bodyToMono(Group.class)
                .block();
    }

    public String deleteGroup(String id) {
        return webClient.build()
                .delete()
                .uri("http://localhost:8084/groups/delete/" + id)
                .header("keycloak", "Bearer " + token)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }

    public void addPersonToGroup(String personId, String groupId) {
        webClient.build()
                .post()
                .uri("http://localhost:8084/groups/addMember/" + groupId + "/" + personId)
                .header("keycloak", "Bearer " + token)
                .retrieve()
                .bodyToMono(String.class)
                .block();
    }
}
