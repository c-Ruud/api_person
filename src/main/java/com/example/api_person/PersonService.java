package com.example.api_person;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class PersonService {

    Map<String, Person> persons = new HashMap<>();
    WebClientHandler webClientHandler;

    public PersonService(WebClientHandler webClientHandler) {
        this.webClientHandler = webClientHandler;
    }

    public Person createPerson(String firstname, String lastname) {
        String id = UUID.randomUUID().toString();
        Person person = new Person(id, firstname, lastname, new ArrayList<>());
        persons.put(id, person);
        return person;
    }

    public Stream<Person> getPersons() {
        return persons.values().stream();
    }

    public Person getById(String id) {
        return persons.get(id);
    }

    public Person updatePerson(String id, String firstName, String lastName) {
        Person person = persons.get(id);
        person.setFirstName(firstName);
        person.setLastName(lastName);
        return person;
    }

    public String deletePerson(String id) throws PersonException {
        boolean doExist = persons.containsKey(id);
        if (!doExist) throw new PersonException("Person not found");
        persons.remove(id);
        return "Person with id: " + id + " was successfully deleted";
    }

    public Person addGroupToPerson(String personId, String groupId) {
        Person person = persons.get(personId);
        webClientHandler.addPersonToGroup(personId, groupId);
        person.addGroup(groupId);
        return person;
    }
}


//   WebClient.Builder webClient; + constructor borttaget också
//detta ska ligga på rad 49, men det fungerar inte som det ska, & eftersom vi inte ska överkomplicera uppgiften så släpper jag detta nu
//        webClient.build()
//                .delete()
//                .uri("http://localhost:8084/groups/removeMember/" + id)
//                .retrieve()
//                .bodyToMono(String.class).block();
//kan inte köra delete om inte gruppen är borta? varför?
//personen försvinner från gruppen,
// men det måste vara ngt fel med svaret? för den ger ett 500 fel
// och utför inte resten från rad 82 och neråt
