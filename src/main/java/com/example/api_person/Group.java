package com.example.api_person;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Group {
    String id;
    String name;
    List<String> personsId;

    @JsonCreator
    public Group(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("personId") List<String> personsId) {
        this.id = id;
        this.name = name;
        this.personsId = personsId;
    }
}
