package com.example.api_person;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ApiPersonApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiPersonApplication.class, args);
    }

/*    @Bean
    public CommandLineRunner dummydata(PersonRepository personRepository) {
        return args -> {
            personRepository.save(new PersonEntity("1", "anna", "anka", null));
            personRepository.save(new PersonEntity("2", "anders", "olsson", null));
            personRepository.save(new PersonEntity("3", "anon", "anonym", null));

        };
    }*/
}
