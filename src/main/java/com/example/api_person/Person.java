package com.example.api_person;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Person {
    String id;
    String firstName;
    String lastName;
    List<String> groups;

    @JsonCreator
    public Person(
            @JsonProperty("id") String id,
            @JsonProperty("firstname") String firstName,
            @JsonProperty("lastname") String lastName,
            @JsonProperty("groups") List<String> groups
    ) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.groups = groups;
    }

    public void addGroup(String group) {
        groups.add(group);
    }
}
