package com.example.api_person;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class KeyCloakToken {
    String accessToken;
    int expiresIn;
    int refreshExpiresIn;
    String refreshToken;
    String tokenType;
    int notBeforePolicy;
    String sessionState;
    String scope;

    @JsonCreator
    public KeyCloakToken(@JsonProperty("access_token") String accessToken,
                         @JsonProperty("expires_in") int expiresIn,
                         @JsonProperty("refresh_expires_in") int refreshExpiresIn,
                         @JsonProperty("refresh_token") String refreshToken,
                         @JsonProperty("token_type") String tokenType,
                         @JsonProperty("not-before-policy") int notBeforePolicy,
                         @JsonProperty("session_state") String sessionState,
                         @JsonProperty("scope") String scope) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.refreshExpiresIn = refreshExpiresIn;
        this.refreshToken = refreshToken;
        this.tokenType = tokenType;
        this.notBeforePolicy = notBeforePolicy;
        this.sessionState = sessionState;
        this.scope = scope;
    }
}
