package com.example.api_person;

public class PersonException extends Exception {
    public PersonException(String message){
        super(message);
    }
}
