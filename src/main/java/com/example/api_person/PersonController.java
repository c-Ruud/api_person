package com.example.api_person;


import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping()
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PersonController {
    PersonService personService;

    WebClientHandler webClientHandler;

    @GetMapping("/getToken")
    public Mono<KeyCloakToken> getToken(@RequestParam String name, @RequestParam String password) {
        return webClientHandler.acquire("http://localhost:8000/", "group-test", "test-client", name, password);
    }

    @PostMapping("/person/create")
    public Person createPerson(@RequestParam String firstName, @RequestParam String lastname) {
        return personService.createPerson(firstName, lastname);
    }

    @PostMapping("/group/create")
    public Group createGroup(@RequestParam String name) {
        return webClientHandler.createGroup(name);
    }

    @GetMapping("/person/get/{id}")
    public Person getPersonById(@PathVariable String id) {
        return personService.getById(id);
    }

    @GetMapping("/group/get/{id}")
    public Group getGroupById(@PathVariable String id) {
        return webClientHandler.getGroupById(id);
    }

    @PutMapping("/person/update/{id}")
    public Person updatePerson(@PathVariable String id, @RequestBody String firstName, String lastName) {
        return personService.updatePerson(id, firstName, lastName);
    }

    @PutMapping("/group/update/{id}")
    public Group updateGroup(@PathVariable String id, @RequestParam String newName) {
        return webClientHandler.updateGroup(id, newName);
    }

    @DeleteMapping("/person/delete/{id}")
    public String deletePerson(@PathVariable String id) throws PersonException {
        return personService.deletePerson(id);
    }

    @DeleteMapping("/group/delete/{id}")
    public String deleteGroup(@PathVariable String id) {
        return webClientHandler.deleteGroup(id);
    }

    @GetMapping("/person/getPersons")
    public List<Person> getAllPersons() {
        return personService.getPersons().collect(Collectors.toList());
    }

    @GetMapping("group/getGroups")
    public List<Group> getAllGroups() {
        return webClientHandler.getGroups();
    }

    @PostMapping("/addGroup/{personId}")
    public Person addGroup(@PathVariable String personId, @RequestParam String groupId) {
        return personService.addGroupToPerson(personId, groupId);
    }
}
